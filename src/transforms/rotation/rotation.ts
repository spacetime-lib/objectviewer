/**
 * Value range (from -180 to 180)
 * Usually used to define an angle
 */
export type PlusToMinus180 = number

export type Rotation = PlusToMinus180[]
