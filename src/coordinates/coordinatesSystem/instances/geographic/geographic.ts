import { CoordinatesSystem } from '../../../'

export const GeographicCoordinatesSystem: CoordinatesSystem = {
  dimensions: ['longitude', 'latitude']
}
