import { CoordinatesSystem } from '../../../'

export const XYCoordinatesSystem: CoordinatesSystem = {
  dimensions: ['abscissa', 'ordinate']
}
