import { GeographicCoordinatesSystem } from '../../../../coordinates/coordinatesSystem/instances'
import { Position, Rotation } from '../../../../transforms'
import { NavigatableSurface } from '../../../'
import { CircleUtil } from './circle.util'

export class NavigatableSphereSurface implements NavigatableSurface {
  coordinatesSystem = GeographicCoordinatesSystem

  protected circle: CircleUtil

  constructor(protected radius: number) {
    this.circle = new CircleUtil(radius)
  }

  getNavigatorNewPositionAndRotation(
    oldPosition: Position,
    oldRotation: Rotation,
    offsetX: number,
    offsetY: number
  ): [Position, Rotation] {
    const newPosition = this.getNavigatorNewPosition(oldPosition, offsetX, offsetY)
    const newRotation = this.getNavigatorNewRotation(oldRotation, newPosition, offsetX, offsetY)

    return [newPosition, newRotation]
  }

  getNavigatorNewPosition(currentPosition: Position, offsetX: number, offsetY: number): Position {
    const [currentLongitude, currentLatitude] = currentPosition

    const newPosition = [
      offsetX
        ? CircleUtil.modulo180(currentLongitude + this.circle.lengthToAngle(offsetX))
        : currentLongitude,
      offsetY
        ? CircleUtil.modulo180(currentLatitude + this.circle.lengthToAngle(offsetY))
        : currentLatitude
    ]

    return newPosition
  }

  /**
   * @return Rotation = [ [angle on x-axis], [angle on y-axis] ]
   */
  getNavigatorNewRotation(
    oldRotation: Rotation,
    newPosition: Position,
    offsetX: number,
    offsetY: number
  ): Rotation {
    const [oldRotationX, oldRotationY] = oldRotation
    const [newLongitude, newLatitude] = newPosition

    /**
     * For an easier reasoning,
     * think of a circle, instead of a sphere.
     * The navigator is just like an ant on the circumference of the circle,
     * it could be facing clockwise, or anti-clockwise, depending of where is he coming from.
     *
     * A Sphere is like a circle in 3D, (i know it's not that simple, but bear with me)
     * so we do our calculation for a circle, but 2 times, 1 for each axis (x-axis, y-axis).
     */

    const newRotation = [
      CircleUtil.modulo180(newLongitude + (offsetX > 0 ? 90 : -90)),
      CircleUtil.modulo180(newLatitude + (offsetY > 0 ? 90 : -90))
    ]

    return newRotation
  }
}
