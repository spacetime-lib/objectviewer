import { CircleUtil } from './circle.util'

describe('CircleUtil', () => {
  describe('lengthToAngle', () => {
    const radius = 100
    let circle = new CircleUtil(radius)

    it('should                       0 =>   0', () => expect(circle.lengthToAngle(0)).toBe(0))
    it('should  this.circumference     => 360', () =>
      expect(circle.lengthToAngle(circle.circumference)).toBe(360))
    it('should  this.circumference / 2 => 180', () =>
      expect(circle.lengthToAngle(circle.circumference / 2)).toBe(180))
    it('should  this.circumference * 2 => 720', () =>
      expect(circle.lengthToAngle(circle.circumference * 2)).toBe(720))
    it('should -this.circumference / 2 => 180', () =>
      expect(circle.lengthToAngle(-circle.circumference / 2)).toBe(-180))
  })

  describe('perpendicularOf', () => {
    it('should (   0, anti-clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(0, false)).toBe(90))
    it('should (   0,      clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(0, true)).toBe(-90))
    it('should (  90, anti-clockwise) =>  180', () =>
      expect(CircleUtil.perpendicularOf(90, false)).toBe(180))
    it('should (  90,      clockwise) =>    0', () =>
      expect(CircleUtil.perpendicularOf(90, true)).toBe(0))
    it('should ( 180, anti-clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(180, false)).toBe(-90))
    it('should ( 180,      clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(180, true)).toBe(90))
    it('should ( 270, anti-clockwise) =>    0', () =>
      expect(CircleUtil.perpendicularOf(270, false)).toBe(0))
    it('should ( 270,      clockwise) =>  180', () =>
      expect(CircleUtil.perpendicularOf(270, true)).toBe(180))
    it('should ( 360, anti-clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(360, false)).toBe(90))
    it('should ( 360,      clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(360, true)).toBe(-90))
    it('should ( 450, anti-clockwise) =>  180', () =>
      expect(CircleUtil.perpendicularOf(450, false)).toBe(180))
    it('should ( 450,      clockwise) =>    0', () =>
      expect(CircleUtil.perpendicularOf(450, true)).toBe(0))
    it('should ( 720, anti-clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(720, false)).toBe(90))
    it('should ( 720,      clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(720, true)).toBe(-90))

    it('should (  -0, anti-clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(-0, false)).toBe(90))
    it('should (  -0,      clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(-0, true)).toBe(-90))
    it('should ( -90, anti-clockwise) =>    0', () =>
      expect(CircleUtil.perpendicularOf(-90, false)).toBe(0))
    it('should ( -90,      clockwise) =>  180', () =>
      expect(CircleUtil.perpendicularOf(-90, true)).toBe(180))
    it('should (-180, anti-clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(-180, false)).toBe(-90))
    it('should (-180,      clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(-180, true)).toBe(90))
    it('should (-270, anti-clockwise) =>  180', () =>
      expect(CircleUtil.perpendicularOf(-270, false)).toBe(180))
    it('should (-270,      clockwise) =>    0', () =>
      expect(CircleUtil.perpendicularOf(-270, true)).toBe(0))
    it('should (-360, anti-clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(-360, false)).toBe(90))
    it('should (-360,      clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(-360, true)).toBe(-90))
    it('should (-450, anti-clockwise) =>    0', () =>
      expect(CircleUtil.perpendicularOf(-450, false)).toBe(0))
    it('should (-450,      clockwise) =>  180', () =>
      expect(CircleUtil.perpendicularOf(-450, true)).toBe(180))
    it('should (-720, anti-clockwise) =>   90', () =>
      expect(CircleUtil.perpendicularOf(-720, false)).toBe(90))
    it('should (-720,      clockwise) =>  -90', () =>
      expect(CircleUtil.perpendicularOf(-720, true)).toBe(-90))
  })

  describe('modulo180', () => {
    it('should    0 =>    0', () => expect(CircleUtil.modulo180(0)).toBe(0))
    it('should   -0 =>    0', () => expect(CircleUtil.modulo180(-0)).toBe(0))
    it('should   90 =>   90', () => expect(CircleUtil.modulo180(90)).toBe(90))
    it('should  -90 =>  -90', () => expect(CircleUtil.modulo180(-90)).toBe(-90))
    it('should  180 =>  180', () => expect(CircleUtil.modulo180(180)).toBe(180))
    it('should -180 =>  180', () => expect(CircleUtil.modulo180(-180)).toBe(180))
    it('should  270 =>  -90', () => expect(CircleUtil.modulo180(270)).toBe(-90))
    it('should -270 =>   90', () => expect(CircleUtil.modulo180(-270)).toBe(90))
    it('should  360 =>    0', () => expect(CircleUtil.modulo180(360)).toBe(0))
    it('should -360 =>    0', () => expect(CircleUtil.modulo180(-360)).toBe(0))
    it('should  450 =>   90', () => expect(CircleUtil.modulo180(450)).toBe(90))
    it('should -450 =>  -90', () => expect(CircleUtil.modulo180(-450)).toBe(-90))
    it('should  720 =>    0', () => expect(CircleUtil.modulo180(720)).toBe(0))
    it('should -720 =>    0', () => expect(CircleUtil.modulo180(-720)).toBe(0))
  })
})
