import { NavigatableSphereSurface } from './sphere'
import { Rotation, Position } from '../../../../transforms'

describe('NavigatableSphereSurface', () => {
  describe('getNavigatorNewPositionAndRotation', () => {
    it('should call getNavigatorNewPosition', () => {
      const nav = new NavigatableSphereSurface(100 /* random radius */)

      const spy = spyOn(nav, 'getNavigatorNewPosition').and.callThrough()

      const theCurrentPosition: Position = []
      const theCurrentRotation: Rotation = []
      const theOffsetX = 10 // random
      const theOffsetY = 40 // random

      nav.getNavigatorNewPositionAndRotation(
        theCurrentPosition,
        theCurrentRotation,
        theOffsetX,
        theOffsetY
      )

      expect(spy).toHaveBeenCalledWith(theCurrentPosition, theOffsetX, theOffsetY)
    })

    it('should call getNavigatorNewRotation', () => {
      const nav = new NavigatableSphereSurface(100 /* random radius */)

      const newPosition: Position = []
      spyOn(nav, 'getNavigatorNewPosition').and.returnValue(newPosition)
      const spy = spyOn(nav, 'getNavigatorNewRotation').and.callThrough()

      const theCurrentPosition: Position = []
      const theCurrentRotation: Rotation = []
      const theOffsetX = 10 // random
      const theOffsetY = 40 // random

      nav.getNavigatorNewPositionAndRotation(
        theCurrentPosition,
        theCurrentRotation,
        theOffsetX,
        theOffsetY
      )

      expect(spy).toHaveBeenCalledWith(theCurrentRotation, newPosition, theOffsetX, theOffsetY)
    })

    it('should return the getNavigatorNewPosition and getNavigatorNewRotation results as a tuple', () => {
      const nav = new NavigatableSphereSurface(100 /* random radius */)

      const newPosition: Position = []
      const newRotation: number[] = []
      spyOn(nav, 'getNavigatorNewPosition').and.returnValue(newPosition)
      spyOn(nav, 'getNavigatorNewRotation').and.returnValue(newRotation)

      const theCurrentPosition: Position = []
      const theCurrentRotation: Rotation = []
      const theOffsetX = 10 // random
      const theOffsetY = 40 // random

      const result = nav.getNavigatorNewPositionAndRotation(
        theCurrentPosition,
        theCurrentRotation,
        theOffsetX,
        theOffsetY
      )

      expect(result).toEqual([newPosition, newRotation])
    })
  })

  describe('getNavigatorNewPosition', () => {
    it('should not modify the longitude if offsetX is falsy', () => {
      const nav = new NavigatableSphereSurface(100 /* random radius */)

      const theLongitude = 10 // random
      const theLatitude = 40 // random
      const theCurrentPosition = [theLongitude, theLatitude]

      const result = nav.getNavigatorNewPosition(
        theCurrentPosition,
        0 /* important */,
        20 /* random */
      )

      expect(result[0]).toBe(theLongitude)
    })
    it('should not modify the latitude if offsetY is falsy', () => {
      const nav = new NavigatableSphereSurface(100 /* random radius */)

      const theLongitude = 10 // random
      const theLatitude = 40 // random
      const theCurrentPosition = [theLongitude, theLatitude]

      const result = nav.getNavigatorNewPosition(
        theCurrentPosition,
        20 /* random */,
        0 /* important */
      )

      expect(result[1]).toBe(theLatitude)
    })
    it('should calculate the new longitude', () => {
      const nav = new NavigatableSphereSurface(
        0.5 /* make circumference exactly PI, for easy calculations */
      )

      const theLongitude = 10 // random
      const theLatitude = 40 // random
      const theCurrentPosition = [theLongitude, theLatitude]

      const theOffsetX = Math.PI / 4 // 90°

      const result = nav.getNavigatorNewPosition(theCurrentPosition, theOffsetX, 20 /* random */)

      expect(result[0]).toBe(100 /* (theLongitude: 10)  +  (theOffsetX: 90) */)
    })
    it('should calculate the new latitude', () => {
      const nav = new NavigatableSphereSurface(
        0.5 /* make circumference exactly PI, for easy calculations */
      )

      const theLongitude = 10 // random
      const theLatitude = 40 // random
      const theCurrentPosition = [theLongitude, theLatitude]

      const theOffsetY = Math.PI / 4 // 90°

      const result = nav.getNavigatorNewPosition(
        theCurrentPosition,
        0 /* faster test */,
        theOffsetY
      )

      expect(result[1]).toBe(130 /* (theLatitude: 40)  +  (theOffsetY: 90) */)
    })
  })
  describe('getNavigatorNewRotation', () => {
    describe('multi-axis movement', () => {
      it('should calculate the new x-axis rotation, 0° -> 90°', () => {
        const oldRotation: Rotation = [0, 0]
        const theOffsetX = 90
        const dummy_offsetY = 0
        const newPosition = [90, 0] // it was [0,0] and became [90,0] because of theOffsetX = 90

        const nav = new NavigatableSphereSurface(100 /* random */)

        const result = nav.getNavigatorNewRotation(
          oldRotation,
          newPosition,
          theOffsetX,
          dummy_offsetY
        )

        expect(result[0]).toBe(180)
      })
    })
    describe('anti-clockwise', () => {
      describe('x-axis', () => {
        it('should calculate the new x-axis rotation, 0° -> 90°', () => {
          const oldPosition: Position = [0, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = 90
          const dummy_offsetY = 0
          const newPosition = [90, 0] // it was [0,0] and became [90,0] because of theOffsetX = 90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(180)
        })
        it('should calculate the new x-axis rotation, 90° -> 180°', () => {
          const oldPosition: Position = [90, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = 90
          const dummy_offsetY = 0
          const newPosition = [180, 0] // it was [90,0] and became [180,0] because of theOffsetX = 90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(-90)
        })
        it('should calculate the new x-axis rotation, 180° -> -90°', () => {
          const oldPosition: Position = [180, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = 90
          const dummy_offsetY = 0
          const newPosition = [-90, 0] // it was [180,0] and became [-90,0] because of theOffsetX = 90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(0)
        })
        it('should calculate the new x-axis rotation, -90° -> 0°', () => {
          const oldPosition: Position = [-90, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = 90
          const dummy_offsetY = 0
          const newPosition = [0, 0] // it was [-90,0] and became [0,0] because of theOffsetX = 90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(90)
        })
      })
      describe('y-axis', () => {
        it('should calculate the new y-axis rotation, 0° -> 90°', () => {
          const oldPosition: Position = [0, 0]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = 90
          const newPosition = [0, 90]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(180)
        })
        it('should calculate the new y-axis rotation, 90° -> 180°', () => {
          const oldPosition: Position = [0, 90]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = 90
          const newPosition = [0, 180]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(-90)
        })
        it('should calculate the new y-axis rotation, 180° -> -90°', () => {
          const oldPosition: Position = [0, 180]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = 90
          const newPosition = [0, -90]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(0)
        })
        it('should calculate the new y-axis rotation, -90° -> 0°', () => {
          const oldPosition: Position = [0, -90]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = 90
          const newPosition = [0, 0]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(90)
        })
      })
    })
    describe('clockwise', () => {
      describe('x-axis', () => {
        it('should calculate the new x-axis rotation, 0° -> -90°', () => {
          const oldPosition: Position = [0, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = -90
          const dummy_offsetY = 0
          const newPosition = [-90, 0] // it was [0,0] and became [-90,0] because of theOffsetX = -90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(180)
        })
        it('should calculate the new x-axis rotation, -90° -> 180°', () => {
          const oldPosition: Position = [-90, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = -90
          const dummy_offsetY = 0
          const newPosition = [180, 0] // it was [-90,0] and became [180,0] because of theOffsetX = -90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(90)
        })
        it('should calculate the new x-axis rotation, 180° -> 90°', () => {
          const oldPosition: Position = [180, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = -90
          const dummy_offsetY = 0
          const newPosition = [90, 0] // it was [180,0] and became [90,0] because of theOffsetX = -90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(0)
        })
        it('should calculate the new x-axis rotation, 90° -> 0°', () => {
          const oldPosition: Position = [90, 0]
          const oldRotation: Rotation = [0, 0]
          const theOffsetX = -90
          const dummy_offsetY = 0
          const newPosition = [0, 0] // it was [0,0] and became [-90,0] because of theOffsetX = -90

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            theOffsetX,
            dummy_offsetY
          )

          expect(result[0]).toBe(-90)
        })
      })
      describe('y-axis', () => {
        it('should calculate the new y-axis rotation, 0° -> -90°', () => {
          const oldPosition: Position = [0, 0]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = -90
          const newPosition = [0, -90]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(180)
        })
        it('should calculate the new y-axis rotation, -90° -> 180°', () => {
          const oldPosition: Position = [0, -90]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = -90
          const newPosition = [0, 180]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(90)
        })
        it('should calculate the new y-axis rotation, 180° -> 90°', () => {
          const oldPosition: Position = [0, 180]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = -90
          const newPosition = [0, 90]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(0)
        })
        it('should calculate the new y-axis rotation, 90° -> 0°', () => {
          const oldPosition: Position = [0, 90]
          const oldRotation: Rotation = [0, 0]
          const dummy_offsetX = 0
          const theOffsetY = -90
          const newPosition = [0, 0]

          const nav = new NavigatableSphereSurface(100 /* random */)

          const result = nav.getNavigatorNewRotation(
            oldRotation,
            newPosition,
            dummy_offsetX,
            theOffsetY
          )

          expect(result[1]).toBe(-90)
        })
      })
    })
  })
})
