import { PlusToMinus180 } from '../../../../transforms'

export class CircleUtil {
  circumference: number

  constructor(public radius: number) {
    this.circumference = CircleUtil.circumferenceOf(radius)
  }

  /**
   * "angle" of what ? Longitude and Latitude are angles
   */
  lengthToAngle(offset: number): number {
    return (offset / this.circumference) * 360
  }

  static circumferenceOf(radius: number): number {
    return 2 * Math.PI * radius
  }

  /**
   *
   * @example
   * ( 0, anti-clockwise) =>   90
   * ( 0,      clockwise) =>  -90
   * (90, anti-clockwise) =>  180
   * (90,      clockwise) =>    0
   */
  static perpendicularOf(angle: PlusToMinus180, clockwise: boolean): PlusToMinus180 {
    const moduledAngle = CircleUtil.modulo180(angle)
    const perpendicular = CircleUtil.modulo180(moduledAngle + (clockwise ? -90 : 90))

    return perpendicular
  }

  /**
   * when n > 180, it restart upward from -180
   *
   * @example
   *    0 =>    0
   *   -0 =>    0
   *   90 =>   90
   *  -90 =>  -90
   *  180 =>  180
   * -180 =>  180
   *  270 =>  -90
   * -270 =>   90
   *  360 =>    0
   * -360 =>    0
   *  450 =>   90
   * -450 =>  -90
   *  720 =>    0
   * -720 =>    0
   */
  static modulo180(n: number): number {
    const abs = Math.abs(n)

    return abs <= 180 ? (abs % 180 ? n : abs) : (Math.floor(n / 180) % 2 ? -1 : 1) * (abs % 180)
  }
}
