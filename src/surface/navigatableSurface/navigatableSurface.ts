import { CoordinatesSystem } from '../../coordinates'
import { Position, Rotation } from '../../transforms'

export interface NavigatableSurface {
  coordinatesSystem: CoordinatesSystem

  getNavigatorNewPositionAndRotation(
    oldPosition: Position,
    oldRotation: Rotation,
    offsetX: number,
    offsetY: number
  ): [Position, Rotation]
}
