import { NavigatableSurface } from '../navigatableSurface/navigatableSurface'
import { Position, Rotation } from '../../transforms'

export class SurfaceNavigator {
  currentSurfacePosition: Position
  currentRotation: Rotation

  constructor(public surface: NavigatableSurface) {
    this.currentSurfacePosition = surface.coordinatesSystem.dimensions.map(d => 0)
    this.currentRotation = surface.coordinatesSystem.dimensions.map(d => 0) // Not sure about this, in NavigatableSphereSurface, we only use 2 axis
  }

  move(offsetX: number, offsetY: number): this {
    ;[
      this.currentSurfacePosition,
      this.currentRotation
    ] = this.surface.getNavigatorNewPositionAndRotation(
      this.currentSurfacePosition,
      this.currentRotation,
      offsetX,
      offsetY
    )

    return this
  }
}
