import { SurfaceNavigator, NavigatableSurface } from '..'

describe('SurfaceNavigator', () => {
  describe('constructor', () => {
    it('should initialize currentSurfacePosition', () => {
      const theSurface = ({
        coordinatesSystem: {
          dimensions: ['x', 'y', 'z']
        }
      } as unknown) as NavigatableSurface

      const nav = new SurfaceNavigator(theSurface)

      expect(nav).toHaveProperty('currentSurfacePosition', [0, 0, 0])
    })

    it('should initialize currentRotation', () => {
      const theSurface = ({
        coordinatesSystem: {
          dimensions: ['x', 'y', 'z']
        }
      } as unknown) as NavigatableSurface

      const nav = new SurfaceNavigator(theSurface)

      expect(nav).toHaveProperty('currentRotation', [0, 0, 0])
    })
  })
  describe('move', () => {
    it('should set currentSurfacePosition', () => {
      const theSurface = ({
        coordinatesSystem: {
          dimensions: ['x', 'y', 'z']
        },
        getNavigatorNewPositionAndRotation: jest.fn(() => [
          [1, 2, 3],
          [4, 5, 6]
        ])
      } as unknown) as NavigatableSurface

      const nav = new SurfaceNavigator(theSurface)

      const result = nav.move(10 /* dummy */, 10 /* dummy */)

      expect(result).toHaveProperty('currentSurfacePosition', [1, 2, 3])
    })
    it('should set currentRotation', () => {
      const theSurface = ({
        coordinatesSystem: {
          dimensions: ['x', 'y', 'z']
        },
        getNavigatorNewPositionAndRotation: jest.fn(() => [
          [1, 2, 3],
          [4, 5, 6]
        ])
      } as unknown) as NavigatableSurface

      const nav = new SurfaceNavigator(theSurface)

      const result = nav.move(10 /* dummy */, 10 /* dummy */)

      expect(result).toHaveProperty('currentRotation', [4, 5, 6])
    })
    it('should call surface.getNavigatorNewPositionAndRotation', () => {
      const theSurface = ({
        coordinatesSystem: {
          dimensions: ['x', 'y', 'z']
        },
        getNavigatorNewPositionAndRotation: jest.fn(() => [
          [1, 2, 3],
          [4, 5, 6]
        ])
      } as unknown) as NavigatableSurface

      const nav = new SurfaceNavigator(theSurface)

      const theOffsetX = 10
      const theOffsetY = 10

      nav.move(theOffsetX, theOffsetY)

      expect(theSurface.getNavigatorNewPositionAndRotation).toHaveBeenCalledWith(
        [0, 0, 0],
        [0, 0, 0],
        theOffsetX,
        theOffsetY
      )
    })
  })
})
