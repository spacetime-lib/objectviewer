import { SurfaceNavigator, NavigatableSurface } from './surface'

export default class ObjectViewer {
  constructor(
    public surface: NavigatableSurface,
    public navigator = new SurfaceNavigator(surface)
  ) {}

  navigate(offsetX: number, offsetY: number): this {
    this.navigator.move(offsetX, offsetY)

    return this
  }
}
